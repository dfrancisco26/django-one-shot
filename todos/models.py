from django.db import models


# Create your models here.
class ToDoList(models.Model):
    name = models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class ToDoItem(models.Model):
    task = models.CharField(max_length=150)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(ToDoList, on_delete=models.CASCADE)

    def __str__(self):
        return self.task
