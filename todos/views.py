from django.shortcuts import render
from todos.models import ToDoList, ToDoItem


# Create your views here.
def todo_list_list(request):
    lists = ToDoList.objects.all()
    context = {
        "todo_list_list": lists,
    }
    return render(request, "todos/list.html", context)


# def items_in_list(request):
#     item_list = ToDoItem.objects.all()
#     context = {"todo_item_list": item_list}
#     return render(request, "", context)
